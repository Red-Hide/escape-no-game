def make_crc_table():
    poly = 0x8408
    table = []
    for byte in range(256):
        crc = 0
        for bit in range(8):
            if (byte ^ crc) & 1:
                crc = (crc >> 1) ^ poly
            else:
                crc >>= 1
            byte >>= 1
        table.append(crc)
    return table

table = make_crc_table()

def crc_16_fast(msg):
    crc = 0xffff
    for byte in msg:
        crc = table[(byte ^ crc) & 0xff] ^ (crc >> 8)
    return crc ^ 0xffff

# Test


msg = bytes.fromhex("61")
print(msg)
out = crc_16_fast(msg)
hi, lo = out >> 8, out & 0xff
print('{:04x} = {:02x} {:02x}'.format(out, hi, lo))

print(bin(int("101000",2)^int("110101",2))[2:].zfill(6))
#print(CRC("110101"))
print(chr(int(''.join(["0","1","0","0","0","0","0","1"]),2)))

fillename = str("Texte.txt")
with open(fillename) as file:
    data = file.read()
    print(data)


class CRC:

    def __init__(self):
        self.cdw = ''

    def xor(self, a, b):
        result = []
        for i in range(1, len(b)):
            if a[i] == b[i]:
                result.append('0')
            else:
                result.append('1')

        return ''.join(result)

    def crc(self, message, key):
        pick = len(key)

        tmp = message[:pick]

        while pick < len(message):
            if tmp[0] == '1':
                tmp = self.xor(key, tmp) + message[pick]
            else:
                tmp = self.xor('0' * pick, tmp) + message[pick]

            pick += 1

        if tmp[0] == "1":
            tmp = self.xor(key, tmp)
        else:
            tmp = self.xor('0' * pick, tmp)

        checkword = tmp
        return checkword

    def encodedData(self, data, key):
        l_key = len(key)
        append_data = data + '0' * (l_key - 1)
        remainder = self.crc(append_data, key)
        codeword = data + remainder
        self.cdw += codeword
        print("Remainder: ", remainder)
        print("Data: ", codeword)

    def reciverSide(self, data, key):
        r = self.crc(data, key)
        size = len(key)
        print(r)
        if r == (size-1) * '0':
            print("No Error")
        else:
            print("Error")


data = '1010001101'
key = '100111100'
c = CRC()
c.encodedData(data, key)
print('---------------')
c.reciverSide(c.cdw, key)
print('---------------')

def manchester_encode(data: str):
    M_bin = []
    for i in data:
        print(type(i))
        if i == "0":
            M_bin.append(0)
            M_bin.append(1)
        else:
            M_bin.append(1)
            M_bin.append(0)
    return M_bin

print(manchester_encode("01010101"))

data = [0,1,0,1,1,1,1,1]
bit = data[0:3]
for i in range(0,len(bit)):
    data.pop(i)

print(data)