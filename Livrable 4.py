import numpy as np
import matplotlib.pyplot as plt
import soundfile as sf
import sounddevice as sd
import scipy
import time

start_Byte = "01011111"
agent_ID = bin(5)[2:].zfill(8)
end_Byte = "01011111"

class CRC:

    def __init__(self):
        self.cdw = ''

    def xor(self, a, b):
        result = []
        for i in range(1, len(b)):
            if a[i] == b[i]:
                result.append('0')
            else:
                result.append('1')

        return ''.join(result)

    def crc(self, message, key):
        pick = len(key)

        tmp = message[:pick]

        while pick < len(message):
            if tmp[0] == '1':
                tmp = self.xor(key, tmp) + message[pick]
            else:
                tmp = self.xor('0' * pick, tmp) + message[pick]

            pick += 1

        if tmp[0] == "1":
            tmp = self.xor(key, tmp)
        else:
            tmp = self.xor('0' * pick, tmp)

        checkword = tmp
        return checkword

    def encodedData(self, data, key):
        l_key = len(key)
        append_data = data + '0' * (l_key - 1)
        remainder = self.crc(append_data, key)
        codeword = data + remainder
        self.cdw += codeword
        print("Remainder: ", remainder)
        print("Data: ", codeword)
        return remainder

    def receiverSide(self, data, key):
        r = self.crc(data, key)
        size = len(key)
        print(r)
        if r == (size - 1) * '0':
            return 1
        else:
            return 0

c = CRC()


def InsertTrame(data: str, extension: int, packet_Index, Fe):
    trame = start_Byte + agent_ID + packet_Index + bin(extension)[2:].zfill(2) + bin(Fe)[2:].zfill(16) + data.zfill(6088) + c.encodedData(data,"100111100") + end_Byte
    return trame


def ExportTrame(donnees: list):
    start = donnees[:8]
    for i in range(0, len(start)):
        donnees.pop(0)
    ID = donnees[:8]
    for i in range(0, len(ID)):
        donnees.pop(0)
    packet = donnees[:16]
    for i in range(0, len(packet)):
        donnees.pop(0)
    extension = donnees[:2]
    for i in range(0, len(extension)):
        donnees.pop(0)
    Fe = donnees[:16]
    for i in range(0, len(Fe)):
        donnees.pop(0)
    end = donnees[-8:]
    for i in range(0, len(end)):
        donnees.pop(-1)
    CRC = donnees[-8:]
    for i in range(0, len(CRC)):
        donnees.pop(-1)
    return start, ID, packet, extension,Fe, donnees, CRC, end

def list_to_str(lst: list):
    chaine = [str(i) for i in lst]
    string = ''.join(chaine)
    return string


def str_to_list(string: str):
    lst = [i for i in string]
    return lst


def str_to_bin(msg: str):
    msg_ascii = [ord(i) for i in msg]
    msg_bin = [bin(i)[2:].zfill(8) for i in msg_ascii]
    return ''.join(msg_bin)


def bin_to_str(msg: list):
    msg_int = [int(''.join(msg[i:i + 8]), 2) for i in range(0, len(msg),8)]
    msg_str = [chr(i) if i != 0 else "" for i in msg_int]
    return ''.join(msg_str)


def signal_to_bin(data):
    result = [bin(i)[2:].zfill(16) if i >= 0 else format(int(i) & 0xffff, '16b').zfill(16) for i in data]
    return result

def bin_to_signal(data):
    result = [int(''.join(data[i:i+16]),2) if data[i]=="0" else int(''.join(data[i+1:i+16]),2)-2**15 for i in range(0, len(data),16)]
    return result

def modulation_ASK(M: list):
    Fe = 192000  # Fréquence d'échantillonnage
    Fp = 96000  # Fréquence de l'onde porteuse

    baud = 96000  # Débit sur le canal de transmission exprimé en bit/s
    Nbits = len(M)  # Nombre de bits initial (taille du message M)
    Ns = int(Fe / baud)  # Nombre de symboles par bit (Fréq d'echan / Débit binaire)
    N = Ns * Nbits  # Nombre de bits total à moduler (Nombre de symboles par bit * Nombre de bits

    # On génère le message binaire dupliqué
    M_duplique = np.repeat(M, Ns)

    # On génère le vecteur temps
    t = np.linspace(0, N / Fe, int(N))

    # On génère la porteuse P(t)
    Porteuse = np.sin(2 * np.pi * Fp * t)

    # On réalise la modualtion en amplitude
    ASK = Porteuse * M_duplique

    return ASK


def demodulation_ASK(ASK):
    Fe = 192000  # Fréquence d'échantillonnage
    Fp = 96000

    baud = 96000  # Débit sur le canal de transmission exprimé en bit/s
    Ns = int(Fe / baud)  # Nombre de symboles par bit (Fréq d'echan / Débit binaire)
    Nbits = int(len(ASK)/Ns)  # Nombre de bits initial (taille du message M)
    N = Ns * Nbits

    t = np.linspace(0, N / Fe, N)  # A COMPLETER - vecteur temps

    Porteuse = np.sin(2 * np.pi * Fp * t)

    Produit = ASK * Porteuse

    Res = []

    for i in range(0, N, Ns):
        Res.extend([1 if np.trapz(Produit[i:i + Ns], t[i:i + Ns]) > 0 else 0])

    return Res


def manchester_decode(signal: list):
    res = ""
    for i in range(0, len(signal) - 1, 2):
        if signal[i] == 0:
            if signal[i + 1] == 1:
                res += "0"
            else:
                res += "1"
        elif signal[i] == 1:
            if signal[i + 1] == 0:
                res += "1"
            else:
                res += "0"
    return res


def manchester_encode(data: str):
    M_bin = []
    for i in data:
        if i == "0":
            M_bin.append(0)
            M_bin.append(1)
        else:
            M_bin.append(1)
            M_bin.append(0)
    return M_bin


def client(type: int):
    if type == 1:
        fillename = str(input("Entrez le nom de votre fichier : ") + ".txt")
        with open(fillename) as file:
            data = file.read()
        data = str_to_bin(data)
        if len(data) <= 12176:
            Trame = InsertTrame(data, type, bin(int(0 / 12176))[2:].zfill(16), 0)
            Trame_manchester = manchester_encode(Trame)
            Signal = modulation_ASK(Trame_manchester)
            sf.write('output_client.wav', Signal, 192000)
        else:
            Signal_total = []
            for i in range(0, len(data) - 1, 12176):
                Trame = InsertTrame(data[i:i + 12176], type, bin(int(i / 12176))[2:].zfill(16), 0)
                Trame_manchester = manchester_encode(Trame)
                Signal_total += Trame_manchester
            Signal = modulation_ASK(Signal_total)
            sf.write('output_client.wav', Signal, 192000)

    elif type == 2:
        filename = str(input("Entrez le nom de votre fichier : ")) + ".wav"
        data, Fe = sf.read(filename, dtype='int16')
        data_bin = signal_to_bin(data)
        data_str = list_to_str(data_bin)
        if len(data_str) <= 12176:
            Trame = InsertTrame(data_str, type, bin(int(0 / 12176))[2:].zfill(16), Fe)
            Trame_manchester = manchester_encode(Trame)
            Signal = modulation_ASK(Trame_manchester)
            sf.write('output_client.wav', Signal, 192000)
        else:
            Signal_total = []
            for i in range(0,len(data_str)-1,12176):
                Trame = InsertTrame(data_str[i:i+12176], type, bin(int(i/12176))[2:].zfill(16), Fe)
                Trame_manchester = manchester_encode(Trame)
                Signal_total += Trame_manchester
            Signal = modulation_ASK(Signal_total)
            sf.write('output_client.wav', Signal, 192000)

    elif type == 3:
        filename = str(input("Entrez le nom de votre fichier : ") + ".txt")

        with open(filename) as file:
            data = file.read()
        if len(data) <= 12176:
            Trame = InsertTrame(data, type, bin(int(0 / 12176))[2:].zfill(16), 0)
            Trame_manchester = manchester_encode(Trame)
            Signal = modulation_ASK(Trame_manchester)
            sf.write('output_client.wav', Signal, 192000)
        else:
            Signal_total = []
            for i in range(0, len(data) - 1, 12176):
                Trame = InsertTrame(data[i:i + 12176], type, bin(int(i / 12176))[2:].zfill(16), 0)
                Trame_manchester = manchester_encode(Trame)
                Signal_total += Trame_manchester
            Signal = modulation_ASK(Signal_total)
            sf.write('output_client.wav', Signal, 192000)


def serveur():
    received_data, Fe = sf.read("output_client.wav",dtype="int16")
    data = demodulation_ASK(received_data)
    data = manchester_decode(data)
    if len(data)<= 12242:
        startB, ag_ID, packetN, extension,Fe, donnees, CRC_remainder, endB = ExportTrame(str_to_list(data))
    else:
        startB = []
        ag_ID = []
        packetN = []
        donnees = []
        CRC_remainder = []
        endB = []
        for i in range(0, len(data) - 1, 12242):
            a, b, d, extension,Fe, e, f, g = ExportTrame(str_to_list(data[i:i+12242]))
            startB += a
            ag_ID += b
            packetN += d
            donnees += e
            CRC_remainder += f
            if c.receiverSide(list_to_str(e + f), "100111100"):
                print("No error")
            else:
                print("Error found\nStopping execution")
                return
            endB += g
    extension = int(list_to_str(extension),2)
    Fe = int(list_to_str(Fe),2)
    donnees = list_to_str(donnees)
    if extension == 1:
        with open("output_serveur.txt","w+") as file:
            file.write(bin_to_str(donnees))
    elif extension == 2:
        donnees = bin_to_signal(donnees)
        donnees = np.array(donnees,dtype=np.int16)
        sf.write("output_serveur.wav",donnees,Fe,"PCM_16")
    elif extension == 3:
        with open("output_serveur.txt","w+") as file:
            file.write(''.join(donnees))


def main():
    if int(input("Que voulez-vous faire :\n1. Envoyer un fichier\n2. Recevoir un Fichier\n")) == 1:
        type_fich = int(input("Entrez le type de fichier : \n1. Texte\n2. Fichier Audio\n3. Fichier Binaire\n"))
        client(type_fich)
    else:
        serveur()


if __name__ == "__main__":
    main()
