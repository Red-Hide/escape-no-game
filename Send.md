---
title: "Send"
author: [Ferrand Guillaume]
keywords: [Projet]
date: \today{}
lang: fr
colorlinks: true
numbersections: true
secnumdepth: 3
table-use-row-colors: true
---
## Partie 1 : Chaîne de transmission  
  
### Conception de la chaîne de transmission  
  
Nous avons mis au point la chaîne de transmission suivante.  
  
![Schéma de la chaîne de transmission](Fichiers/Livrable%203.drawio.png)  
  
#### Conversion en données binaires  
  
Pour que les données atteignes le destinataire nous les convertissons d'abord en données binaires.  
  
##### Conversion d'un fichier son  
  
Pour un fichier son nous allons donc tout d'abord normaliser le son dans un format codé sur 16 bits et avec une fréquence d'échantillonnage de 44100Hz permettant de limiter la dégradation du son lors de la conversion. Nous allons ensuite récupérer ces valeurs et les "traduire" en binaire. Chaque valeur prendra donc 2 octets.  
  
##### Conversion d'un texte ASCII  
  
Pour un fichier texte il faudra passer des différents caractères à leur valeur binaire pour pouvoir les encoder et les envoyer. Pour cela nous allons nous réferer à la table ASCII suivante.  
  
![Table ASCII](Fichiers/ASCII-Table.svg.png)  
  
Pour un fichier binaire il n'y a rien à faire pour l'instant car les données sont déjà sous forme binaire de 0 et de 1.  
  
#### Encodage des données  
  
Nous allons ensuite devoir encoder ces données via un algorithme d'encodage. Pour cela nous avons plusieurs choix comme le codage NRZ (Non Return to Zero) ou bien le codage Manchester.  
  
##### Codage NRZ  
  
Dans notre cas nous allons utiliser le codage manchester car le codage NRZ a beaucoup plus d'inconvénients que le codage Manchester.  
Le codage NRZ est synchrone. La synchronisation sera difficile voire impossible dans le cas où on a de longues séquences de 0 ou de 1.  
  
##### Codage Manchester  
  
Pour le manchester cependant le principe est de diviser chaque bit en 2 (1 bit donnera 2 bits). Le 0 est représenté par un front descendant (de haut à bas) et le 1 sera représenté par un front montant (de bas à haut). Nous allons donc prendre le codage manchester car il comporte peu d'inconvénients.  
  
#### Modulation du signal  
  
Nous avons plusieurs options pour la modulation du signal. La modulation ASK(Modulation d'amplitude), FSK(Modulation de fréquence) et PSK(Modulation de phase).  
  
#### Transmission des données dans le réseau  
  
Ce signal est ensuite envoyé par le haut-parleur du mini PAD pour ensuite être réceptionné par le microphone et transporté dans le réseau que notre agent sera en train de surveiller.